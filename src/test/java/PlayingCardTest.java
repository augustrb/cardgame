import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.model.PlayingCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class PlayingCardTest {

    private PlayingCard card1;
    private PlayingCard card2;

  @BeforeEach
  void setUp() {
    card1 = new PlayingCard('H', 2);
    card2 = new PlayingCard('S', 6);
  }


  @Nested
  @DisplayName("Testing the constructor")
  class constructorTests {
    @Test
    @DisplayName("Testing the constructor with a heart and 14 throws IllegalArgumentException")
    void testConstructorTooHighFaceException() {
      assertThrows(IllegalArgumentException.class, () -> {
        new PlayingCard('H', 14);
      });
    }

    @Test
    @DisplayName("Testing the constructor with a heart and -1 throws IllegalArgumentException")
    void testConstructorTooLowFaceException() {
      assertThrows(IllegalArgumentException.class, () -> {
        new PlayingCard('H', -1);
      });
    }

    @Test
    @DisplayName("Testing the constructor with a heart and 1 does not throw IllegalArgumentException")
    void testConstructorLowestFace() {
      Assertions.assertDoesNotThrow(() -> {
        new PlayingCard('H', 1);
      });
    }

    @Test
    @DisplayName("Testing the constructor with invalid suit and 6 throws IllegalArgumentException")
    void testConstructorInvalidSuitException() {
      assertThrows(IllegalArgumentException.class, () -> {
        new PlayingCard('X', 6);
      });
    }

    @Test
    @DisplayName("Testing the constructor with a valid suit and 6 does not throw IllegalArgumentException")
    void testConstructorValidSuit() {
      Assertions.assertDoesNotThrow(() -> {
        new PlayingCard('H', 6);
      });
    }
  }

  @Nested
  class getTests {
    @Test
    @DisplayName("Testing the getSuit method")
    void testGetSuit() {
      assertEquals('H', card1.getSuit());
    }

    @Test
    @DisplayName("Testing the getFace method")
    void testGetFace() {
      assertEquals(2, card1.getFace());
    }

    @Test
    @DisplayName("Testing the getAsString method")
    void testGetAsString() {
      assertEquals("H2", card1.getAsString());
    }
  }

  @Nested
  class equalsTests {
    @Test
    @DisplayName("Testing the equals method with two equal cards")
    void testEquals() {
      PlayingCard card3 = new PlayingCard('H', 2);
      assertEquals(card1, card3);
    }

    @Test
    @DisplayName("Testing the equals method with two different cards")
    void testNotEquals() {
      Assertions.assertNotEquals(card1, card2);
    }
  }

}

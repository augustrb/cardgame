import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.model.DeckOfCards;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {

  private DeckOfCards deck;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @Nested
  @DisplayName("Testing getCards method")
  class getTests {
    @Test
    @DisplayName("Testing getCards method")
    void testGetCards() {
      assertEquals(52, deck.getCards().size());
    }

    @Test
    @DisplayName("Testing getCards method after removing a card")
    void testGetCardsAfterRemove() {
      deck.removeCard(0);
      assertEquals(51, deck.getCards().size());
    }

    @Test
    @DisplayName("Testing getCards method after removing all cards")
    void testGetCardsAfterRemoveAll() {
      for (int i = 0; i < 52; i++) {
        deck.removeCard(0);
      }
      assertEquals(0, deck.getCards().size());
    }
  }

  @Nested
  @DisplayName("Testing the removeCard method")
    class removeCardTests {
      @Test
      @DisplayName("Testing the removeCard method")
      void testRemoveCard() {
      deck.removeCard(0);
      assertEquals(51, deck.getCards().size());
      }

      @Test
      @DisplayName("Testing the removeCard method with an index out of bounds")
      void testRemoveCardOutOfBounds() {
      assertThrows(IndexOutOfBoundsException.class, () -> deck.removeCard(52));
      }
    }
}

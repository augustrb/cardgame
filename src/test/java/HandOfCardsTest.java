import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.model.HandOfCards;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class HandOfCardsTest {
  private HandOfCards hand;

  @BeforeEach
  void setUp() {
      hand = new HandOfCards();
  }

  @Nested
  @DisplayName("Testing the dealHand method")
  class constructorTests {
    @Test
    @DisplayName("Testing the dealHand method with a negative number of cards throws IllegalArgumentException")
    void testConstructorNegativeCardsException() {
      assertThrows(IllegalArgumentException.class, () -> {
        hand.dealHand(-1);
      });
    }

    @Test
    @DisplayName("Testing the dealHand method with a positive number of cards does not throw IllegalArgumentException")
    void testConstructorPositiveCards() {
      Assertions.assertDoesNotThrow(() -> {
        hand.dealHand(5);
      });
    }
  }

}

package edu.ntnu.stud.model;

import static java.util.stream.StreamSupport.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class HandOfCards {
  private final ArrayList<PlayingCard> handOfCards;

  public HandOfCards() {
    handOfCards = new ArrayList<>();
  }

  public ArrayList<PlayingCard> getHandOfCards() {
      return handOfCards;
  }

  public void dealHand(int n) {
    DeckOfCards freshDeck = new DeckOfCards();
    if (n < 0 || n >= freshDeck.getCards().size()) {
      throw new IllegalArgumentException("Invalid number of cards: " + n);
    }
    Random rand = new Random();

    for (int i = 0; i < n; i++) {
      int amountOfCards = freshDeck.getCards().size();
      int cardIndex = rand.nextInt(amountOfCards);
      handOfCards.add(freshDeck.getCards().get(cardIndex));
      freshDeck.removeCard(cardIndex);
    }
  }

  public int sumOfHand() {
    return handOfCards.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }

  public String cardsOfHearts() {
    return handOfCards.stream()
        .filter(card -> card.getSuit() == 'H')
        .map(PlayingCard::getAsString)
        .collect(Collectors.joining(" "));
  }

  public boolean hasQueenOfSpades() {
    return handOfCards.stream()
        .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
  }

  public boolean hasFlush() {
    return handOfCards.stream()
        .collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()))
        .values()
        .stream()
        .anyMatch(count -> count >= 5);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (PlayingCard card : handOfCards) {
      sb.append(card.getAsString()).append(" ");
    }
    return sb.toString();
  }

}

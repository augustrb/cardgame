package edu.ntnu.stud.model;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {

  private final ArrayList<PlayingCard> cards;

  public DeckOfCards() {
    cards = new ArrayList<>();
    char[] suit = {'S', 'H', 'D', 'C'};
    for (int s = 0; s < 4; s++) {
      for (int n = 1; n <= 13; n++) {
          cards.add(new PlayingCard(suit[s], n));
      }
    }
  }

  public void removeCard(int index) {
    cards.remove(index);
  }

  public ArrayList<PlayingCard> getCards() {
    return cards;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (PlayingCard card : cards) {
      sb.append(card.getAsString()).append("\n");
    }
    return sb.toString();
  }
}

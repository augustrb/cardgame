package edu.ntnu.stud.launcher;

import edu.ntnu.stud.model.HandOfCards;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainUI extends Application {

  Stage window;

  Scene scene;

  HandOfCards hand;

  @Override
  public void start(Stage primaryStage) {
    window = primaryStage;

    Label label1 = new Label("Welcome to a card game!");
    Label cardDisplayLabel = new Label();
    TextField amountInput = new TextField();
    amountInput.setPromptText("Amount of cards");
    amountInput.setMaxWidth(100);

    Button dealButton = new Button("Deal Hand");
    dealButton.setOnAction(e -> {
        try {
          int amount = Integer.parseInt(amountInput.getText());
          hand = new HandOfCards();
          hand.dealHand(amount);
          cardDisplayLabel.setText(hand.toString());
        } catch (NumberFormatException ex) {
          cardDisplayLabel.setText("Please enter a valid number");
        } catch (IllegalArgumentException ex) {
          cardDisplayLabel.setText("Please enter a positive number");
        }
    });

    Label sumFaceLabel = new Label("Sum of face values: ");
    Label cardsOfHeartsLabel = new Label("Cards of hearts: ");
    Label flushLabel = new Label("Flush: ");
    Label queenOfSpadesLabel = new Label("Queen of spades: ");

    Button checkButton = new Button("Check Hand");
    checkButton.setOnAction(e -> {
      try {
        sumFaceLabel.setText("Sum of face values: " + hand.sumOfHand());
        flushLabel.setText("Flush: " + hand.hasFlush());
        queenOfSpadesLabel.setText("Queen of spades: " + hand.hasQueenOfSpades());
        cardsOfHeartsLabel.setText("Cards of hearts: " + hand.cardsOfHearts());
      } catch (NullPointerException ex) {
        cardDisplayLabel.setText("Please deal a hand first");
      }
    });

    VBox centerMenu = new VBox(20);
    centerMenu.getChildren().addAll(cardDisplayLabel);
    centerMenu.setAlignment(Pos.CENTER);

    VBox leftMenu = new VBox(20);
    leftMenu.getChildren().addAll(sumFaceLabel, cardsOfHeartsLabel, flushLabel, queenOfSpadesLabel);
    leftMenu.setAlignment(Pos.CENTER);
    leftMenu.setPadding(new Insets(20, 20, 20, 20));;

    VBox rightMenu = new VBox(20);
    rightMenu.getChildren().addAll(amountInput, dealButton, checkButton);
    rightMenu.setAlignment(Pos.CENTER);
    rightMenu.setPadding(new Insets(20, 20, 20, 20));

    VBox bottomMenu = new VBox(20);
    bottomMenu.getChildren().addAll(label1);
    bottomMenu.setAlignment(Pos.CENTER);

    BorderPane layout1 = new BorderPane();
    layout1.setRight(rightMenu);
    layout1.setLeft(leftMenu);
    layout1.setBottom(bottomMenu);
    layout1.setCenter(centerMenu);

    scene = new Scene(layout1, 1000, 800);

    window.setScene(scene);
    window.show();

  }
}
